@[Link(framework: "OpenGL")]
lib GL
  TRUE                 = 0x0001u32
  FALSE                = 0x0000u32

  COLOR_BUFFER_BIT     = 0x4000u32
  DEPTH_BUFFER_BIT     = 0x0100u32
  DEPTH_TEST           = 0x0B71u32
  BLEND                = 0x0BE2u32
  FRONT                = 0x0404u32
  BACK                 = 0x0405u32
  FRONT_AND_BACK       = 0x0408u32
  POINT                = 0x1B00u32
  LINE                 = 0x1B01u32
  FILL                 = 0x1B02u32
  CULL_FACE            = 0x0B44u32
  CW                   = 0x0900u32
  CCW                  = 0x0901u32

  TEXTURE_2D           = 0x0DE1u32
  TEXTURE0             = 0x84C0u32
  COLOR_INDEX          = 0x1900u32
  STENCIL_INDEX        = 0x1901u32
  DEPTH_COMPONENT      = 0x1902u32
  RED                  = 0x1903u32
  GREEN                = 0x1904u32
  BLUE                 = 0x1905u32
  ALPHA                = 0x1906u32
  RGB                  = 0x1907u32
  RGBA                 = 0x1908u32
  LUMINANCE            = 0x1909u32
  LUMINANCE_ALPHA      = 0x190Au32
  TEXTURE_MAG_FILTER   = 0x2800u32
  TEXTURE_MIN_FILTER   = 0x2801u32
  TEXTURE_WRAP_S       = 0x2802u32
  TEXTURE_WRAP_T       = 0x2803u32
  NEAREST              = 0x2600u32
  LINEAR               = 0x2601u32
  CLAMP                = 0x2900u32
  REPEAT               = 0x2901u32

  ZERO                 = 0x0000u32
  ONE                  = 0x0001u32
  SRC_COLOR            = 0x0300u32
  ONE_MINUS_SRC_COLOR  = 0x0301u32
  SRC_ALPHA            = 0x0302u32
  ONE_MINUS_SRC_ALPHA  = 0x0303u32
  DST_ALPHA            = 0x0304u32
  ONE_MINUS_DST_ALPHA  = 0x0305u32

  VERTEX_SHADER        = 0x8B31u32
  FRAGMENT_SHADER      = 0x8B30u32
  COMPILE_STATUS       = 0x8B81u32
  INFO_LOG_LENGTH      = 0x8B84u32

  ARRAY_BUFFER         = 0x8892u32
  ELEMENT_ARRAY_BUFFER = 0x8893u32
  STATIC_DRAW          = 0x88E4u32

  BYTE                 = 0x1400u32
  UNSIGNED_BYTE        = 0x1401u32
  SHORT                = 0x1402u32
  UNSIGNED_SHORT       = 0x1403u32
  INT                  = 0x1404u32
  UNSIGNED_INT         = 0x1405u32
  FLOAT                = 0x1406u32
  DOUBLE               = 0x140Au32

  POINTS               = 0x0000u32
  LINES                = 0x0001u32
  LINE_LOOP            = 0x0002u32
  LINE_STRIP           = 0x0003u32
  TRIANGLES            = 0x0004u32
  TRIANGLE_STRIP       = 0x0005u32
  TRIANGLE_FAN         = 0x0006u32
  QUADS                = 0x0007u32
  QUAD_STRIP           = 0x0008u32
  POLYGON              = 0x0009u32

  NO_ERROR             = 0u32
  INVALID_ENUM         = 0x0500u32
  INVALID_VALUE        = 0x0501u32
  INVALID_OPERATION    = 0x0502u32
  STACK_OVERFLOW       = 0x0503u32
  STACK_UNDERFLOW      = 0x0504u32
  OUT_OF_MEMORY        = 0x0505u32

  VERSION              = 0x1F02u32

  fun get_error    = glGetError : UInt32
  fun get_string   = glGetString(name : UInt32) : UInt8*

  fun enable         = glEnable(cap : UInt32)
  fun disable        = glDisable(cap : UInt32)
  fun clear_color    = glClearColor(r : Float32, g : Float32, b : Float32, a : Float32)
  fun clear          = glClear(mask : UInt32)
  fun polygon_mode   = glPolygonMode(face : UInt32, mode : UInt32)
  fun blend_func     = glBlendFunc(sfactor : UInt32, dfactor : UInt32)
  fun cull_face      = glCullFace(mode : UInt32)
  fun front_face     = glFrontFace(mode : UInt32)
  fun viewport       = glViewport(x : Int32, y : Int32, width : Int32, height : Int32)
  fun flush          = glFlush

  fun create_shader  = glCreateShader(type : UInt32) : UInt32
  fun shader_source  = glShaderSource(shader : UInt32, count : Int32, string : UInt8**, length : Int32*)
  fun compile_shader = glCompileShader(shader : UInt32)
  fun get_shader_iv  = glGetShaderiv(shader : UInt32, pname : UInt32, params : Int32*)
  fun get_shader_info_log = glGetShaderInfoLog(shader : UInt32, bufSize : Int32, length : Int32*, infoLog : UInt8*)
  fun create_program = glCreateProgram : UInt32
  fun attach_shader  = glAttachShader(program : UInt32, shader : UInt32)
  fun link_program   = glLinkProgram(program : UInt32)
  fun use_program    = glUseProgram(program : UInt32)

  fun gen_vertex_arrays = glGenVertexArrays(n : Int32, arrays : UInt32*)
  fun bind_vertex_array = glBindVertexArray(array : UInt32)
  fun gen_buffers       = glGenBuffers(n : Int32, buffers : UInt32*)
  fun bind_buffer       = glBindBuffer(target : UInt32, buffer : UInt32)
  fun buffer_data       = glBufferData(target : UInt32, size : UInt32, data : Void*, usage : UInt32)

  fun get_attrib_location        = glGetAttribLocation(program : UInt32, name : UInt8*) : Int32
  fun enable_vertex_attrib_array = glEnableVertexAttribArray(index : UInt32)
  fun vertex_attrib_pointer      = glVertexAttribPointer(index : UInt32, size : Int32, type : UInt32, normalized : Int32, stride : Int32, pointer : Void*)
  fun get_uniform_location       = glGetUniformLocation(program : UInt32, name : UInt8*) : Int32
  fun uniform_matrix_4fv         = glUniformMatrix4fv(location : Int32, count : Int32, transpose : Int32, value : Float32*)

  fun gen_textures    = glGenTextures(n : Int32, textures : UInt32*)
  fun active_texture  = glActiveTexture(texture : UInt32)
  fun bind_texture    = glBindTexture(target : UInt32, texture : UInt32)
  fun tex_image_2D    = glTexImage2D(target : UInt32, level : Int32, internal_format : Int32, width : Int32, height : Int32, border : Int32, format : UInt32, type : UInt32, pixels : Void*)
  fun tex_parameter_i = glTexParameteri(target : UInt32, pname : UInt32, param : Int32)

  fun draw_arrays   = glDrawArrays(mode : UInt32, first : Int32, count : Int32)
  fun draw_elements = glDrawElements(mode : UInt32, count : Int32, type : UInt32, indices : Void*)

end
