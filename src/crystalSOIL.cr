@[Link("soil")]
lib SOIL
  fun load_image = SOIL_load_image(name : UInt8*, texture_width : Int32*, texture_height : Int32*, texture_channels : Int32*, type : Int32) : UInt8*

  LOAD_AUTO = 0
  LOAD_L    = 1
  LOAD_LA   = 2
  LOAD_RGB  = 3
  LOAD_RGBA = 4
end
