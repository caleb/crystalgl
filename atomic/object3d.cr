class Object3d
  def initialize(
    @position = Vector3::ZERO : Vector3,
    @scale = Vector3::ONE : Vector3,
    @rotation = Vector3::ZERO : Vector3,
    @colour = Colour::WHITE : Colour,
    @shine = 5.0 : Float64,
    @alpha = 1.0 : Float64
  )
    @vao = 0u32
    @vertex_vbo = 0u32

    @vertices = [
      Vertex.new,
      Vertex.new(Vector3.new(0, 0, 10)),
      Vertex.new(Vector3.new(10, 0, 0))
    ]

    @enabled_attributes = {
      :position => Slice(Float32).new(0),
      :colour   => Slice(Float32).new(0)
    }

    @shader = 0u32
  end

  def enable_attribute(attribute)
    @enabled_attributes[attribute] = Slice(Float32).new(0) unless attribute_enabled? attribute
  end

  def disable_attribute(attribute)
    #TODO: Free attribute slice before delete (?)
    @enabled_attributes.delete(attribute)
  end

  def attribute_enabled?(attribute)
    @enabled_attributes.has_key? attribute
  end

  private def malloc_attribute(attribute)
    disable_attribute(attribute)
    @enabled_attributes[attribute] = Slice(Float32).new(@vertices.length * 3)
  end

  def bind(shader : UInt32)
    @shader = shader

    GL.gen_vertex_arrays(1, pointerof(@vao))
    GL.bind_vertex_array(@vao)

    if attribute_enabled? :position
      malloc_attribute(:position)
      s_positions = @enabled_attributes[:position]
      p_positions = s_positions.pointer(s_positions.length)
      @vertices.each_with_index do |vertex, i|
        base_index = i*3
        p_positions[base_index]   = vertex.position.x.to_f32
        p_positions[base_index+1] = vertex.position.y.to_f32
        p_positions[base_index+2] = vertex.position.z.to_f32
      end

      GL.gen_buffers(1, pointerof(@vertex_vbo))
      GL.bind_buffer(GL::ARRAY_BUFFER, @vertex_vbo)
      GL.buffer_data(GL::ARRAY_BUFFER, sizeof(Float32).to_u32 * s_positions.length.to_u32, (p_positions as Void*), GL::STATIC_DRAW)

      position_location = GL.get_attrib_location(Shader[:test], "position").to_u32
      GL.enable_vertex_attrib_array(position_location)
      GL.vertex_attrib_pointer(position_location, 3, GL::FLOAT, 0, sizeof(Float32)*3, Pointer(Void).new(0u64))
    end
  end

  def draw

    modelViewProjectionMatrix = Matrix4.symmetricPerspectiveProjection(Math::PI / 2.0, 2.0, 0.01, 1000.0) *
                                Matrix4.camera(Vector3.new(15, 4, 15), Vector3::ZERO, Vector3::UNIT_Y)

    loc = GL.get_uniform_location(@shader, "modelViewProjectionMatrix")
    GL.uniform_matrix_4fv(loc, 1, 1, modelViewProjectionMatrix.to_array_pointer)


    GL.bind_vertex_array(@vao)
    GL.draw_arrays(GL::TRIANGLES, 0, 3)
  end
end
