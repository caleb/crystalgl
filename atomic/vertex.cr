class Vertex
  property :position
  property :normal

  def initialize(
    @position = Vector3::ZERO           : Vector3,
    @normal   = Vector3::UNIT_Y         : Vector3,
    @colour   = Colour::CORNFLOWER_BLUE : Colour,
    @uv       = Vector2::ZERO           : Vector2
  )
  end

  def to_s
    "{\n  Position: #{@position}\n  Normal:   #{@normal}\n  Colour:   #{@colour}\n  uv:       #{@uv}\n}"
  end
end
