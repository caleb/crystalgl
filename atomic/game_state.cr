class NoGameException < Exception end

class GameState
  def initialize
    @camera = Camera3D.new(self)
  end

  def game
    if game = Game.instance
      game
    else
      raise NoGameException.new("Game state is existing without a Game.")
    end
  end

  def starting
  end

  def ending
  end

  def camera
    @camera.not_nil!
  end

  def camera=(value)
    @camera = value
  end

  def update
  end

  def draw
  end
end
