#version 330
uniform mat4 modelViewProjectionMatrix;

in vec3 position;

void main(void)
{
  gl_Position = modelViewProjectionMatrix * vec4(position.x, position.y, position.z, 1.0);
}
