macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

struct Vector3
  not_nil_property :x, Float64
  not_nil_property :y, Float64
  not_nil_property :z, Float64

  def initialize(x = 0.0, y = 0.0, z = 0.0)
    @x = x.to_f64
    @y = y.to_f64
    @z = z.to_f64
  end

  def initialize(v2 : Vector2, z)
    self(v2.x, v2.y, z)
  end

  def +
    self
  end

  def -
    Vector3.new(-x, -y, -z)
  end

  def +(other : Vector3)
    Vector3.new(x + other.x, y + other.y, z + other.z)
  end

  def -(other : Vector3)
    Vector3.new(x - other.x, y - other.y, z - other.z)
  end

  def *(other)
    Vector3.new(x * other, y * other, z * other)
  end

  def /(other)
    Vector3.new(x / other, y / other, z / other)
  end

  def length
    Math.sqrt(length_squared)
  end

  def length_squared
    x*x + y*y + z*z
  end

  def normalized
    self / length
  end

  def normalize!
    l = length
    @x = x / l
    @y = y / l
    @z = z / l
  end

  def dot(other : Vector3)
    x * other.x + y * other.y + z * other.z
  end

  def cross(other : Vector3)
    Vector3.new(y * other.z - z * other.y, (x * other.z - z * other.x) * -1, x * other.y - y * other.x)
  end

  def cross_value(other : Vector3)
    y * other.z - z * other.y + (x * other.z - z * other.x) * -1 + x * other.y - y * other.x
  end

  ZERO   = Vector3.new(0, 0, 0)
  ONE    = Vector3.new(1, 1, 1)
  UNIT_X = Vector3.new(1, 0, 0)
  UNIT_Y = Vector3.new(0, 1, 0)
  UNIT_Z = Vector3.new(0, 0, 1)
end
