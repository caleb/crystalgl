class ShaderCompileException < Exception
end

class UnknownBindingMethodException < Exception
end

class Shader
  @@shaders = {} of Symbol => UInt32

  def self.load(name : Symbol, vertex_filename : String, fragment_filename : String)
    vertex_shader = load_and_compile(vertex_filename, :vertex_shader)
    fragment_shader = load_and_compile(fragment_filename, :fragment_shader)
    shader = GL.create_program
    GL.attach_shader(shader, vertex_shader)
    GL.attach_shader(shader, fragment_shader)
    GL.link_program(shader)

    @@shaders[name] = shader
  end

  def self.activate(name)
    GL.use_program(@@shaders[name])
  end

  def self.[](value)
    @@shaders[value]
  end

  def self.bind_uniform(name, value)
    if value.is_a? Matrix4
      
    else
      throw UnkownBindingMethodException.new("Cannot bind #{name} as it is of an unknown type `#{value.class}`")
    end
  end

  private def self.load_and_compile(filename, type)
    # Load entire file into a string
    file = Pacio.open_for_read(filename)
    contents = file.read_all
    file.close

    # Convert string into an array of uint8 on the heap
    chars = Pointer(UInt8).malloc(contents.length)
    contents.each_char_with_index do |char, i|
      char.each_byte { |byte| chars[i] = byte }
    end

    # Compile shader
    shader = GL.create_shader(type == :vertex_shader ? GL::VERTEX_SHADER : GL::FRAGMENT_SHADER)
    GL.shader_source(shader, 1, pointerof(chars), nil)
    GL.compile_shader(shader)

    # Check for compile erorrs
    rc = Pointer(Int32).malloc(1)
    GL.get_shader_iv(shader, GL::COMPILE_STATUS, rc)

    if rc.value != GL::TRUE
      bufferSize = Pointer(Int32).malloc(1)
      GL.get_shader_iv(shader, GL::INFO_LOG_LENGTH, bufferSize)

      length = Pointer(Int32).malloc(1)
      error = Pointer(UInt8).malloc(bufferSize.value)
      GL.get_shader_info_log(shader, bufferSize.value, length, error)
      error_message = String.new(Slice.new(error, length.value))
      raise ShaderCompileException.new("Failed to compile `#{filename}`:\n #{error_message}")
    end

    shader
  end
end
