class InputState
  def initialize
    @key_states = Array(Bool).new(350, false)
    @mouse_states = Array(Bool).new(10, false)
  end

  def initialize(clone : InputState)
    @key_states = clone.key_states.dup
    @mouse_states = clone.mouse_states.dup
  end

  protected def key_states
    @key_states
  end

  protected def mouse_states
    @mouse_states
  end

  def process_keyboard_event(key, action, mods)
    if action == GLFW::PRESS
      @key_states[key] = true
    elsif action == GLFW::RELEASE
      @key_states[key] = false
    end
  end

  def process_mouse_event(button, action, mods)
    if action == GLFW::PRESS
      @mouse_states[button] = true
    elsif action == GLFW::RELEASE
      @mouse_states[button] = false
    end
  end

  def is_down?(key)
    @key_states[key]
  end

  def is_up?(key)
    !is_down?(key)
  end

  def is_mouse_down?(button)
    @mouse_states[button]
  end

  def is_mouse_up?(button)
    !is_mouse_down?(button)
  end

  def to_s
    key_states.to_s
  end
end

abstract class Input
  @@current_state  = InputState.new
  @@previous_state = InputState.new
  @@typed_text = ""
  @@mouse_position = Vector2::ZERO
  @@mouse_scroll = Vector2::ZERO

  def self.is_key_down?(key : Int32)
    @@current_state.is_down? key
  end

  def self.is_key_up?(key : Int32)
    @@current_state.is_up? key
  end

  def self.is_key_pressed?(key : Int32)
    @@current_state.is_down?(key) && @@previous_state.is_up?(key)
  end

  def self.is_key_released?(key : Int32)
    @@current_state.is_up?(key) && @@previous_state.is_down?(key)
  end

  def self.typed_text
    @@typed_text
  end

  def self.mouse_position
    @@mouse_position
  end

  def self.mouse_scroll
    @@mouse_scroll
  end

  def self.is_mouse_down?(button : Int32)
    @@current_state.is_mouse_down? button
  end

  def self.is_mouse_up?(button : Int32)
    @@current_state.is_mouse_up? button
  end

  def self.is_mouse_pressed?(button : Int32)
    @@current_state.is_mouse_down?(button) && @@previous_state.is_mouse_up?(button)
  end

  def self.is_mouse_released?(button : Int32)
    @@current_state.is_mouse_up?(button) && @@previous_state.is_mouse_down?(button)
  end

  def self.key_event(key, action, mods)
    @@current_state.process_keyboard_event(key, action, mods)
  end

  def self.ascii_event(codepoint)
    @@typed_text += codepoint.chr
  end

  def self.mouse_button_event(button, action, mods)
    @@current_state.process_mouse_event(button, action, mods)
  end

  def self.mouse_move_event(x, y)
    @@mouse_position = Vector2.new(x, y)
  end

  def self.scroll_event(dx, dy)
    @@mouse_scroll += Vector2.new(dx, dy)
  end

  def self.swap_buffers
    @@previous_state = InputState.new(@@current_state)
    @@typed_text = ""
    @@mouse_scroll = Vector2::ZERO
  end
end
