class Window
  def initialize(title, width = 1600, height = 900)
    @handle = GLFW.create_window(width, height, title, nil, nil)
    GLFW.make_context_current(@handle)
  end

  def title=(value)
    GLFW.set_window_title(@handle, value)
  end

  def set_resolution(width, height)
    GLFW.set_window_size(@handle, width, height)
  end

  def get_resolution
    width = Pointer(Int32).malloc(1)
    height = Pointer(Int32).malloc(1)
    GLFW.get_framebuffer_size(@handle, width, height)
    { width: width.value, height: height.value }
  end

  def poll_events
    GLFW.poll_events
  end

  def swap_buffers
    GLFW.swap_buffers(@handle)
  end

  def should_close!
    GLFW.set_window_should_close(@handle, 1)
  end

  def should_close?
    GLFW.window_should_close(@handle) == 1
  end

  def register_key_callback(callback)
    GLFW.set_key_callback(@handle, callback)
  end

  def register_ascii_callback(callback)
    GLFW.set_char_callback(@handle, callback)
  end

  def register_mouse_button_callback(callback)
    GLFW.set_mouse_button_callback(@handle, callback)
  end

  def register_cursor_pos_callback(callback)
    GLFW.set_cursor_pos_callback(@handle, callback)
  end

  def register_scroll_callback(callback)
    GLFW.set_scroll_callback(@handle, callback)
  end
end

GLFW.init
GLFW.window_hint(GLFW::CONTEXT_VERSION_MAJOR, 3)
GLFW.window_hint(GLFW::CONTEXT_VERSION_MINOR, 3)
GLFW.window_hint(GLFW::OPENGL_FORWARD_COMPAT, 1)
GLFW.window_hint(GLFW::OPENGL_PROFILE, GLFW::OPENGL_CORE_PROFILE)
