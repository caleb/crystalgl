class Renderer < Component
  property :mesh

  @shader = 0u32
  @vao    = 0u32
  @vbo    = 0u32

  @enabled_attributes = {
    :position => Slice(Float32).new(0),
    :colour   => Slice(Float32).new(0)
  }

  def setup(params)
    @mesh      = params.fetch(:mesh, @owner.mesh as Mesh)
    @transform = params.fetch(:transform, @owner.transform as Transform)
    @shader    = Shader[params.fetch(:shader, :gouraud)]

    bind
  end

  def transform
    @transform.not_nil!
  end

  def transform=(value : Transform)
    @transform = value
  end

  def vertices
    if mesh = @mesh
      mesh.vertices
    else
      [] of Vertex
    end
  end

  def enable_attribute(attribute)
    @enabled_attributes[attribute] = Slice(Float32).new(0) unless attribute_enabled? attribute
  end

  def disable_attribute(attribute)
    #TODO: Free attribute slice before delete (?)
    @enabled_attributes.delete(attribute)
  end

  def attribute_enabled?(attribute)
    @enabled_attributes.has_key? attribute
  end

  private def malloc_attribute(attribute)
    disable_attribute(attribute)
    @enabled_attributes[attribute] = Slice(Float32).new(vertices.length * 3)
  end

  def bind
    GL.gen_vertex_arrays(1, pointerof(@vao))
    GL.bind_vertex_array(@vao)

    if attribute_enabled? :position
      malloc_attribute(:position)
      s_positions = @enabled_attributes[:position]
      p_positions = s_positions.pointer(s_positions.length)
      vertices.each_with_index do |vertex, i|
        base_index = i*3
        p_positions[base_index]   = vertex.position.x.to_f32
        p_positions[base_index+1] = vertex.position.y.to_f32
        p_positions[base_index+2] = vertex.position.z.to_f32
      end

      GL.gen_buffers(1, pointerof(@vbo))
      GL.bind_buffer(GL::ARRAY_BUFFER, @vbo)
      GL.buffer_data(GL::ARRAY_BUFFER, sizeof(Float32).to_u32 * s_positions.length.to_u32, (p_positions as Void*), GL::STATIC_DRAW)

      position_location = GL.get_attrib_location(@shader, "position").to_u32
      GL.enable_vertex_attrib_array(position_location)
      GL.vertex_attrib_pointer(position_location, 3, GL::FLOAT, 0, sizeof(Float32)*3, Pointer(Void).new(0u64))
    end
  end

  def draw
    modelViewProjectionMatrix = @game_state.camera.projection_matrix * @game_state.camera.view_matrix * Matrix4.translate(transform.position)

    loc = GL.get_uniform_location(@shader, "modelViewProjectionMatrix")
    GL.uniform_matrix_4fv(loc, 1, 1, modelViewProjectionMatrix.to_array_pointer)


    GL.bind_vertex_array(@vao)
    GL.draw_arrays(GL::TRIANGLES, 0, 3)
  end
end
