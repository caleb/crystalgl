class Mesh < Component
  DEFAULT_VERTICES = [
    Vertex.new,
    Vertex.new(Vector3.new(1, 0, 0)),
    Vertex.new(Vector3.new(0, 0, 1)),
    Vertex.new(Vector3.new(1, 0, 0)),
    Vertex.new(Vector3.new(1, 0, 1)),
    Vertex.new(Vector3.new(0, 0, 1))
  ]

  def setup(params)
    @vertices = params.fetch(:vertices, DEFAULT_VERTICES)
  end

  def vertices
    if vertices = @vertices
      vertices
    else
      [] of Vertex
    end
  end
end
