macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

class Transform < Component
  not_nil_property :position, Vector3
  not_nil_property :scale, Vector3
  not_nil_property :rotation, Vector3

  def setup(params)
    @position = params.fetch(:position, Vector3::ZERO)
    @scale    = params.fetch(:scale, Vector3::ONE)
    @rotation = params.fetch(:rotation, Vector3::ZERO)
  end

  def to_s
    "Transform {\n\tPosition: #{@position}\n\tScale: #{@scale}\n\tRotation: #{@rotation}\n}"
  end
end
