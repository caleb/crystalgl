macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

class ViewProjection < Component
  not_nil_property :fov, Float64
  not_nil_property :aspect_ratio, Float64
  not_nil_property :near, Float64
  not_nil_property :far, Float64
  not_nil_property :position, Vector3
  not_nil_property :gaze, Vector3
  not_nil_property :up, Vector3

  def setup(params)
    @fov          = params.fetch(:fov, Math::PI / 2.0)
    @aspect_ratio = params.fetch(:aspect_ratio, 2.0)
    @near         = params.fetch(:near, 0.01)
    @far          = params.fetch(:far, 10000.0)

    @position = params.fetch(:position, Vector3.new(1, 1, 1))
    @up       = params.fetch(:up, Vector3::UNIT_Y)

    @gaze = params.fetch(:gaze, Vector3::ZERO)
    if @gaze == Vector3::ZERO
      look_at(params.fetch(:look_at, Vector3::ZERO))
    else
      calculate_right_and_up
    end
  end

  def look_at(look_at : Vector3)
    @gaze = (look_at - position).normalized
    calculate_right_and_up
  end

  def calculate_right_and_up
    calculate_right
    up     = right.cross(gaze).normalized
  end

  def calculate_right
    right  = gaze.cross(up).normalized
    @right = right
  end

  def right
    if r = @right
      r
    else
      Vector3::ZERO
    end
  end

  def projection_matrix
    fov          = @fov
    aspect_ratio = @aspect_ratio
    near         = @near
    far          = @far
    if fov && aspect_ratio && near && far
      Matrix4.symmetricPerspectiveProjection(fov, aspect_ratio, near, far)
    else
      Matrix4::IDENTITY
    end
  end

  def view_matrix
    Matrix4.camera(position, gaze, right, up)
  end

  def to_s
    "Component:ViewProjection {\n\tPosition: #{position}\n\tGaze: #{gaze}\n\tUp: #{up}\n\t[Right]: #{@right}\n}"
  end
end
