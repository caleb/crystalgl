class Component
  property :owner

  def initialize(@owner, @game_state, params)
    setup(params)
  end

  def setup(params)
  end

  def update
  end

  def draw
  end
end
