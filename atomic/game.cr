abstract class Game
  def initialize
    @window = Window.new("Atomic Game Engine")
    @clear_colour = Colour::CORNFLOWER_BLUE
    Game.instance = self
  end

  def self.instance
    @@instance
  end

  def self.instance=(value)
    @@instance = value
  end

  def change_state(new_state : GameState)
    @current_game_state = new_state
  end

  def window
    @window
  end

  def quit
    @window.should_close!
  end

  def run
    __atomic_register_callbacks

    GL.enable(GL::TEXTURE_2D)
    GL.enable(GL::DEPTH_TEST)
    GL.enable(GL::BLEND)
    GL.enable(GL::CULL_FACE)
    GL.polygon_mode(GL::FRONT_AND_BACK, GL::FILL)
    GL.blend_func(GL::SRC_ALPHA, GL::ONE_MINUS_SRC_ALPHA)
    GL.cull_face(GL::BACK)
    GL.front_face(GL::CW)
    GL.clear_color(@clear_colour.r.to_f32, @clear_colour.g.to_f32, @clear_colour.b.to_f32, @clear_colour.a.to_f32)

    load_resources

    while !@window.should_close?
      res = @window.get_resolution
      resized(res[:width], res[:height])

      update
      display

      Input.swap_buffers
      GLFW.poll_events
    end
  end

  def resized(width, height)
    GL.viewport(0, 0, width, height)
  end

  def update
    if state = @current_game_state
      state.update
    end
  end

  def display
    GL.clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT)
    if state = @current_game_state
      state.draw
    end
    @window.swap_buffers
  end
end

def __atomic_register_callbacks
  if window = Game.instance.try(&.window)
    key_callback = ->(context : Void*, key : Int32, scancode : Int32, action : Int32, mods : Int32) {
      Input.key_event(key, action, mods)
    }
    window.register_key_callback(key_callback)

    ascii_callback = ->(context : Void*, codepoint : UInt32) {
      Input.ascii_event(codepoint)
    }
    window.register_ascii_callback(ascii_callback)

    mouse_button_callback = ->(context : Void*, button : Int32, action : Int32, mods : Int32) {
      Input.mouse_button_event(button, action, mods)
    }
    window.register_mouse_button_callback(mouse_button_callback)

    cursor_pos_callback = ->(context : Void*, xpos : Float64, ypos : Float64) {
      Input.mouse_move_event(xpos, ypos)
    }
    window.register_cursor_pos_callback(cursor_pos_callback)

    scroll_callback = ->(context : Void*, xpos : Float64, ypos : Float64) {
      Input.scroll_event(xpos, ypos)
    }
    window.register_scroll_callback(scroll_callback)
  end
end
