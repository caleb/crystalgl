class GLHelper
  ERRORS = {
    0x0000u32 => "No Error",
    0x0500u32 => "Invalid Enum",
    0x0501u32 => "Invalid Value",
    0x0502u32 => "Invalid Operation",
    0x0503u32 => "Stack Overflow",
    0x0504u32 => "Stack Underflow",
    0x0505u32 => "Out of Memory"
  }

  def self.last_error_code
    GL.get_error
  end

  def self.had_error?
    last_error_code != GL::NO_ERROR
  end

  def self.last_error_string
    error = last_error_code
    return "Unknown error" unless ERRORS.has_key?(error)
    ERRORS[error]
  end

  def self.major_version
    GL.get_string(GL::VERSION).value - 48
  end

  def self.generate_vertex_array_object
    temp = Pointer(UInt32).malloc(1)
    GL.gen_vertex_arrays(1, temp)
    temp.value
  end
end
