macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

struct Colour
  not_nil_property :r, Float64
  not_nil_property :g, Float64
  not_nil_property :b, Float64
  not_nil_property :a, Float64

  def initialize(r, g, b, a = 1.0)
    @r = r.to_f64
    @g = g.to_f64
    @b = b.to_f64
    @a = a.to_f64
  end

  WHITE             = Colour.new(1, 1, 1)
  BLACK             = Colour.new(0, 0, 0)
  TRANSPARENT_WHITE = Colour.new(1, 1, 1, 0)
  TRANSPARENT_BLACK = Colour.new(0, 0, 0, 0)
  RED               = Colour.new(1, 0, 0)
  GREEN             = Colour.new(0, 1, 0)
  BLUE              = Colour.new(0, 0, 1)
  CORNFLOWER_BLUE   = Colour.new(0.39, 0.58, 0.92)
end
