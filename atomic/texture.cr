class Texture
  @@sprites = {} of Symbol => Texture
  @@next_texture = 0u32
  @@resource_directory = ""

  def self.resource_directory=(value)
    @@resource_directory = value
  end

  def initialize(filename)
    @texture_buffer = 0u32
    GL.gen_textures(1, pointerof(@texture_buffer))
    GL.active_texture(GL::TEXTURE0 + @@next_texture)
    GL.bind_texture(GL::TEXTURE_2D, @texture_buffer)

    @texture_width    = 0
    @texture_height   = 0
    @texture_channels = 0
    @image_data = SOIL.load_image(
      "#{@@resource_directory}/#{filename}",
      pointerof(@texture_width),
      pointerof(@texture_height),
      pointerof(@texture_channels),
      SOIL::LOAD_RGB
    )

    GL.tex_image_2D(
      GL::TEXTURE_2D,
      0,
      GL::RGB.to_i,
      @texture_width,
      @texture_height,
      0,
      GL::RGB,
      GL::UNSIGNED_BYTE,
      @image_data as Pointer(Void)
    )

    set_filter_method(:linear)
    set_wrap_method(:repeat)

    @@next_texture += 1
  end

  def set_filter_method(method)
    GL.bind_texture(GL::TEXTURE_2D, @texture_buffer)
    method_const = (method == :linear ? GL::LINEAR : GL::CLAMP).to_i
    GL.tex_parameter_i(GL::TEXTURE_2D, GL::TEXTURE_MAG_FILTER, method_const)
    GL.tex_parameter_i(GL::TEXTURE_2D, GL::TEXTURE_MIN_FILTER, method_const)
  end

  def set_wrap_method(method)
    GL.bind_texture(GL::TEXTURE_2D, @texture_buffer)
    method_const = (method == :repeat ? GL::REPEAT : GL::CLAMP).to_i
    GL.tex_parameter_i(GL::TEXTURE_2D, GL::TEXTURE_WRAP_S, method_const)
    GL.tex_parameter_i(GL::TEXTURE_2D, GL::TEXTURE_WRAP_T, method_const)
  end
end
