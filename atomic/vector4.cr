macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

struct Vector4
  not_nil_property :x, Float64
  not_nil_property :y, Float64
  not_nil_property :z, Float64
  not_nil_property :w, Float64

  def initialize(x, y, z, w)
    @x = x.to_f64
    @y = y.to_f64
    @z = z.to_f64
    @w = w.to_f64
  end

  def initialize(v2 : Vector2, z, w)
    initialize(v2.x, v2.y, z, w)
  end

  def initialize(v3 : Vector3, w)
    initialize(v3.x, v3.y, v3.z, w)
  end

  def +
    self
  end

  def -
    Vector4.new(-x, -y, -z, -w)
  end

  def +(other : Vector4)
    Vector4.new(x + other.x, y + other.y, z + other.z, w + other.w)
  end

  def -(other : Vector4)
    Vector4.new(x - other.x, y - other.y, z - other.z, w - other.w)
  end

  def *(other)
    Vector4.new(x * other, y * other, z * other, w * other)
  end

  def /(other)
    Vector4.new(x / other, y / other, z / other, w / other)
  end

  def length
    Math.sqrt(length_squared)
  end

  def length_squared
    x*x + y*y + z*z + w*w
  end

  def normalized
    self / length
  end

  def normalized!
    l = length
    @x = x / l
    @y = y / l
    @z = z / l
    @w = w / l
  end

  def dot(other : Vector4)
    x * other.x + y * other.y + z * other.z + w * other.w
  end

  ZERO   = Vector4.new(0, 0, 0, 0)
  ONE    = Vector4.new(1, 1, 1, 1)
  UNIT_X = Vector4.new(1, 0, 0, 0)
  UNIT_Y = Vector4.new(0, 1, 0, 0)
  UNIT_Z = Vector4.new(0, 0, 1, 0)
  UNIT_W = Vector4.new(0, 0, 0, 1)
end
