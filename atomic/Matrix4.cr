struct Matrix4
  def initialize(v1 = Vector4::ZERO : Vector4, v2 = Vector4::ZERO : Vector4, v3 = Vector4::ZERO : Vector4, v4 = Vector4::ZERO : Vector4)
    @vm = [v1, v2, v3, v4]
  end

  def v1
    @vm[0]
  end

  def v2
    @vm[1]
  end

  def v3
    @vm[2]
  end

  def v4
    @vm[3]
  end

  def [](i)
    @vm[i]
  end

  def to_s
    "⌜#{v1.x} #{v2.x} #{v3.x} #{v4.x}⌝\n|#{v1.y} #{v2.y} #{v3.y} #{v4.y}|\n|#{v1.z} #{v2.z} #{v3.z} #{v4.z}|\n⌞#{v1.w} #{v2.w} #{v3.w} #{v4.w}⌟"
  end

  def to_array_pointer
    p = Pointer(Float32).malloc(16)
    [
      v1.x.to_f32, v1.y.to_f32, v1.z.to_f32, v1.w.to_f32,
      v2.x.to_f32, v2.y.to_f32, v2.z.to_f32, v2.w.to_f32,
      v3.x.to_f32, v3.y.to_f32, v3.z.to_f32, v3.w.to_f32,
      v4.x.to_f32, v4.y.to_f32, v4.z.to_f32, v4.w.to_f32
    ].each_with_index { |value, i| p[i] = value }
    p
  end

  def *(other : Matrix4)
    m2 = other.transposed
    Matrix4.new(
      Vector4.new(v1.dot(m2.v1), v1.dot(m2.v2), v1.dot(m2.v3), v1.dot(m2.v4)),
      Vector4.new(v2.dot(m2.v1), v2.dot(m2.v2), v2.dot(m2.v3), v2.dot(m2.v4)),
      Vector4.new(v3.dot(m2.v1), v3.dot(m2.v2), v3.dot(m2.v3), v3.dot(m2.v4)),
      Vector4.new(v4.dot(m2.v1), v4.dot(m2.v2), v4.dot(m2.v3), v4.dot(m2.v4))
    )
  end

  def *(other : Vector4)
    Vector4.new(v1.dot(other), v2.dot(other), v3.dot(other), v4.dot(other))
  end

  def *(other : Vector3)
    o4 = Vector4.new(other, 1)
    Vector3.new(v1.dot(o4), v2.dot(o4), v3.dot(o4))
  end

  def transposed
    Matrix4.new(
      Vector4.new(v1.x, v2.x, v3.x, v4.x),
      Vector4.new(v1.y, v2.y, v3.y, v4.y),
      Vector4.new(v1.z, v2.z, v3.z, v4.z),
      Vector4.new(v1.w, v2.w, v3.w, v4.w)
    )
  end

  def inverted
    m = [
      v1.x, v1.y, v1.z, v1.w,
      v2.x, v2.y, v2.z, v2.w,
      v3.x, v3.y, v3.z, v3.w,
      v4.x, v4.y, v4.z, v4.w
    ]
    inv = Array(Float64).new(16, 0.0)

    inv[0] = m[5] * m[10] * m[15] -
      m[5] * m[11] * m[14] -
      m[9] * m[6] * m[15] +
      m[9] * m[7] * m[14] +
      m[13] * m[6] * m[11] -
      m[13] * m[7] * m[10];

    inv[4] = -m[4] * m[10] * m[15] +
      m[4] * m[11] * m[14] +
      m[8] * m[6] * m[15] -
      m[8] * m[7] * m[14] -
      m[12] * m[6] * m[11] +
      m[12] * m[7] * m[10];

    inv[8] = m[4] * m[9] * m[15] -
      m[4] * m[11] * m[13] -
      m[8] * m[5] * m[15] +
      m[8] * m[7] * m[13] +
      m[12] * m[5] * m[11] -
      m[12] * m[7] * m[9];

    inv[12] = -m[4] * m[9] * m[14] +
      m[4] * m[10] * m[13] +
      m[8] * m[5] * m[14] -
      m[8] * m[6] * m[13] -
      m[12] * m[5] * m[10] +
      m[12] * m[6] * m[9];

    inv[1] = -m[1] * m[10] * m[15] +
      m[1] * m[11] * m[14] +
      m[9] * m[2] * m[15] -
      m[9] * m[3] * m[14] -
      m[13] * m[2] * m[11] +
      m[13] * m[3] * m[10];

    inv[5] = m[0] * m[10] * m[15] -
      m[0] * m[11] * m[14] -
      m[8] * m[2] * m[15] +
      m[8] * m[3] * m[14] +
      m[12] * m[2] * m[11] -
      m[12] * m[3] * m[10];

    inv[9] = -m[0] * m[9] * m[15] +
      m[0] * m[11] * m[13] +
      m[8] * m[1] * m[15] -
      m[8] * m[3] * m[13] -
      m[12] * m[1] * m[11] +
      m[12] * m[3] * m[9];

    inv[13] = m[0] * m[9] * m[14] -
      m[0] * m[10] * m[13] -
      m[8] * m[1] * m[14] +
      m[8] * m[2] * m[13] +
      m[12] * m[1] * m[10] -
      m[12] * m[2] * m[9];

    inv[2] = m[1] * m[6] * m[15] -
      m[1] * m[7] * m[14] -
      m[5] * m[2] * m[15] +
      m[5] * m[3] * m[14] +
      m[13] * m[2] * m[7] -
      m[13] * m[3] * m[6];

    inv[6] = -m[0] * m[6] * m[15] +
      m[0] * m[7] * m[14] +
      m[4] * m[2] * m[15] -
      m[4] * m[3] * m[14] -
      m[12] * m[2] * m[7] +
      m[12] * m[3] * m[6];

    inv[10] = m[0] * m[5] * m[15] -
      m[0] * m[7] * m[13] -
      m[4] * m[1] * m[15] +
      m[4] * m[3] * m[13] +
      m[12] * m[1] * m[7] -
      m[12] * m[3] * m[5];

    inv[14] = -m[0] * m[5] * m[14] +
      m[0] * m[6] * m[13] +
      m[4] * m[1] * m[14] -
      m[4] * m[2] * m[13] -
      m[12] * m[1] * m[6] +
      m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] +
      m[1] * m[7] * m[10] +
      m[5] * m[2] * m[11] -
      m[5] * m[3] * m[10] -
      m[9] * m[2] * m[7] +
      m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] -
      m[0] * m[7] * m[10] -
      m[4] * m[2] * m[11] +
      m[4] * m[3] * m[10] +
      m[8] * m[2] * m[7] -
      m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] +
      m[0] * m[7] * m[9] +
      m[4] * m[1] * m[11] -
      m[4] * m[3] * m[9] -
      m[8] * m[1] * m[7] +
      m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] -
      m[0] * m[6] * m[9] -
      m[4] * m[1] * m[10] +
      m[4] * m[2] * m[9] +
      m[8] * m[1] * m[6] -
      m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    return nil if det == 0
    det = 1.0 / det;

    Matrix4.new(
      Vector4.new(inv[0]*det, inv[1]*det, inv[2]*det, inv[3]*det),
      Vector4.new(inv[4]*det, inv[5]*det, inv[6]*det, inv[7]*det),
      Vector4.new(inv[8]*det, inv[9]*det, inv[10]*det, inv[11]*det),
      Vector4.new(inv[12]*det, inv[13]*det, inv[14]*det, inv[15]*det)
    )

  end


  def self.translate(amount : Vector3)
    translate(amount.x, amount.y, amount.z)
  end

  def self.translate(x, y, z)
    Matrix4.new(
      Vector4.new(1, 0, 0, x),
      Vector4.new(0, 1, 0, y),
      Vector4.new(0, 0, 1, z),
      Vector4.new(0, 0, 0, 1)
    )
  end

  def self.scale(x, y, z)
    Matrix4.new(
      Vector4.new(x, 0, 0, 0),
      Vector4.new(0, y, 0, 0),
      Vector4.new(0, 0, z, 0),
      Vector4.new(0, 0, 0, 1)
    )
  end

  def self.rotate(around : Vector3, angle : Float64)
    around.normalize!
    x = around.x
    y = around.y
    z = around.z
    c = Math.cos(angle)
    s = Math.sin(angle)
    Matrix4.new(
      Vector4.new(c + x*x*(1 - c), x*y*(1 - c) - z*s, x*z*(1 - c) + y*s, 0),
      Vector4.new(x*y*(1 - c) + z*s, c + y*y*(1 - c), y*z*(1 - c) - x*s, 0),
      Vector4.new(z*x*(1 - c) - y*s, z*y*(1 - c) + x*s, c + z*z*(1 - c), 0),
      Vector4.new(0, 0, 0, 1)
    )
  end

  def self.camera(position : Vector3, look_at : Vector3, up : Vector3)
    w = (position - look_at).normalized
    u = up.cross(w).normalized
    v = w.cross(u)
    camera(position, w * -1, u, v)
  end

  def self.camera(position : Vector3, gaze : Vector3, right : Vector3, above : Vector3)
    translation_matrix = translate(position * -1)
    rotation_matrix = Matrix4.new(
      Vector4.new(right, 0),
      Vector4.new(above, 0),
      Vector4.new(gaze * -1, 0),
      Vector4.new(0, 0, 0, 1)
    )

    rotation_matrix * translation_matrix
  end

  def self.symmetricPerspectiveProjection(fov : Float64, aspect_ratio : Float64, near : Float64, far : Float64)
    Matrix4.new(
      Vector4.new(1.0 / (Math.tan(fov / 2.0) * aspect_ratio), 0, 0, 0),
      Vector4.new(0, 1.0 / Math.tan(fov / 2.0), 0, 0),
      Vector4.new(0, 0, (-far - near) / (far - near), (-2 * far * near) / (far - near)),
      Vector4.new(0, 0, -1, 0)
    )
  end

  IDENTITY = Matrix4.new(Vector4::UNIT_X, Vector4::UNIT_Y, Vector4::UNIT_Z, Vector4::UNIT_W)
end
