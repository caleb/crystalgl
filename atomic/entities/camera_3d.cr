class Camera3D < Entity
  component :view_projection, ViewProjection

  def view_projection
    get_component(:view_projection) as ViewProjection
  end

  def move(amount)
    view_projection.position += view_projection.gaze * amount
  end

  def rotate_around_vector(rotate_vector, amount)
    rot = Matrix4.rotate(rotate_vector, amount)
    view_projection.gaze = (rot * view_projection.gaze).normalized
    view_projection.up   = (rot * view_projection.up).normalized
    view_projection.calculate_right
  end

  def roll(amount)
    rotate_around_vector(view_projection.gaze, amount)
  end

  def pitch(amount)
    rotate_around_vector(view_projection.right, amount)
  end

  def yaw(amount)
    rotate_around_vector(view_projection.up, amount)
  end

  # Helper Delegation Methods
  def position
    view_projection.position
  end

  def position=(value : Vector3)
    view_projection.position = value
  end

  def look_at(look_at : Vector3)
    view_projection.look_at(look_at)
  end

  def projection_matrix
    view_projection.projection_matrix
  end

  def view_matrix
    view_projection.view_matrix
  end
end
