struct ComponentDefinition
  property :type
  property :params

  def initialize(@type : Class, @params)
  end
end

class UnknownComponentException < Exception end

class Entity
  def self.component(name, type, params = {} of Symbol => Object)
    definition = ComponentDefinition.new(type, params)
    if defs = @@component_definitions
      defs[name] = definition
      @@component_definitions = defs
    else
      @@component_definitions = { name => definition }
    end
  end

  def initialize(@game_state)
    @components = {} of Symbol => Component

    if defs = @@component_definitions
      defs.each do |name, definition|
        add_component(name, definition.type.new(self, @game_state, definition.params))
      end
    end
  end

  def add_component(name, component)
    @components[name] = component
  end

  def get_component(name)
    component = @components[name]?
    raise UnknownComponentException.new("Entity does not have a component `#{name}`") unless component
    component
  end

  def to_s
    "Entity:#{self.class} {\n#{components_to_s}\n}"
  end

  private def components_to_s
    @components.values.map do |component|
      "\t" + component.to_s.gsub(/\n/, "\n\t")
    end.join("\n\n")
  end

  def transform
    get_component(:transform)
  end

  def mesh
    get_component(:mesh)
  end

  def renderer
    get_component(:renderer)
  end
end
