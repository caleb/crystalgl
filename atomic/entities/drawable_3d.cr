class Drawable3D < Entity
  component :transform, Transform
  component :mesh, Mesh
  component :renderer, Renderer
end
