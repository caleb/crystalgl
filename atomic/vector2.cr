macro not_nil_property(name, type)
  def {{name.id}}
    @{{name.id}}.not_nil!
  end
  def {{name.id}}=(value : {{type.id}})
    @{{name.id}} = value
  end
end

struct Vector2
  not_nil_property :x, Float64
  not_nil_property :y, Float64

  def initialize(x = 0.0, y = 0.0)
    @x = x.to_f64
    @y = y.to_f64
  end

  def +
    self
  end

  def -
    Vector2.new(-x, -y)
  end

  def +(other : Vector2)
    Vector2.new(x + other.x, y + other.y)
  end

  def -(other : Vector2)
    Vector2.new(x - other.x, y - other.y)
  end

  def *(other)
    Vector2.new(x * other, y * other)
  end

  def /(other)
    Vector2.new(x / other, y / other)
  end

  def length
    Math.sqrt(length_squared)
  end

  def length_squared
    x*x + y*y
  end

  def normalized
    self / length
  end

  def normalize!
    l = length
    @x = x / l
    @y = y / l
  end

  ZERO   = Vector2.new(0, 0)
  ONE    = Vector2.new(1, 1)
  UNIT_X = Vector2.new(1, 0)
  UNIT_Y = Vector2.new(0, 1)
end
