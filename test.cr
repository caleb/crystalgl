require "./src/crystalGL.cr"
require "./src/crystalGLFW.cr"
require "./atomic/atomic.cr"

class TestState < GameState
  def initialize
    super
    Shader.activate(:gouraud)
    @world_objects = [] of Drawable3D
    @world_objects <<  Drawable3D.new(self)

    @vel = Vector2::ZERO
    @acc = Vector2::ONE

    @texture = Texture.new("sprites/img_test.png")
  end

  def update
    game.quit if Input.is_key_released? GLFW::KEY_ESCAPE
    game.quit if Input.is_mouse_released? GLFW::MOUSE_BUTTON_MIDDLE
  end

  def draw
    @world_objects.each do |world_object|
      (world_object.renderer as Renderer).draw
    end
  end
end

class MyGame < Game
  def initialize
    super
    @window.title = "My Cool Game"
    @window.set_resolution(640, 480)
  end

  def load_resources
    Shader.load(:gouraud, "atomic/shaders/gouraud.vert", "atomic/shaders/gouraud.frag")

    change_state(TestState.new)
  end
end

game = MyGame.new
game.run
